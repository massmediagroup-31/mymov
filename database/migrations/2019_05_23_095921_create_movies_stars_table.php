<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMoviesStarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movies_stars', function (Blueprint $table) {
            $table->bigInteger('movies_id')->unsigned();
            $table->bigInteger('stars_id')->unsigned();

            $table->foreign('movies_id')->references('id')->on('movies')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('stars_id')->references('id')->on('stars')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['movies_id', 'stars_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::table('movies_directors', function (Blueprint $table) {
//            $table->dropForeign('movies_stars_movies_id_foreign');
//            $table->dropForeign('movies_stars_stars_id_foreign');
//        });

        Schema::dropIfExists('movies_stars');
    }
}
