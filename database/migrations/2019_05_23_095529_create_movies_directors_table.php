<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMoviesDirectorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movies_directors', function (Blueprint $table) {
            //$table->bigIncrements('id');
            $table->bigInteger('movies_id')->unsigned();
            $table->bigInteger('directors_id')->unsigned();

            $table->foreign('movies_id')->references('id')->on('movies')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('directors_id')->references('id')->on('directors')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['movies_id', 'directors_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::table('movies_directors', function (Blueprint $table) {
//            $table->dropForeign('movies_id_foreign');
//            $table->dropForeign('directors_id_foreign');
//        });

        Schema::dropIfExists('movies_directors');
    }
}
