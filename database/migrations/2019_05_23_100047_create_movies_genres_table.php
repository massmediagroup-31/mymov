<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMoviesGenresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movies_genres', function (Blueprint $table) {
            $table->bigInteger('movies_id')->unsigned();
            $table->bigInteger('genres_id')->unsigned();

            $table->foreign('movies_id')->references('id')->on('movies')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('genres_id')->references('id')->on('genres')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['movies_id', 'genres_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::table('movies_directors', function (Blueprint $table) {
//            $table->dropForeign('movies_genres_movies_id_foreign');
//            $table->dropForeign('movies_genres_genres_id_foreign');
//        });

        Schema::dropIfExists('movies_genres');
    }
}
