<?php

use Illuminate\Database\Seeder;

class MoviesTableSeeder extends Seeder
{
    public function run()
    {
        factory(App\Models\Movie::class, 10)->create();
    }
}