<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\Movie;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Movie::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'year' => 1999,
        'image_url' => 'https://via.placeholder.com/350x150',
        'certificate' => '16+',
        'runtime' => rand(60, 240),
        'imdb_rating' => rand(1, 9),
        'description' => $faker->realText(rand(80, 600)),
        'metascore' => rand(1, 90),
        'rating' => rand(1, 9),
    ];
});
