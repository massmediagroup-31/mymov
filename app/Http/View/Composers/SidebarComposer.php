<?php

namespace App\Http\View\Composers;

use Illuminate\View\View;
use App\Models\Movie;

class SidebarComposer
{
    public function compose(View $view)
    {
        return $view->with('lastMovies', Movie::inRandomOrder()->take(3)->get());
    }
}