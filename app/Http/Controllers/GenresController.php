<?php

namespace App\Http\Controllers;

use App\Models\Genre;
use Illuminate\Http\Request;

class GenresController extends Controller
{
    public function show($name)
    {
        $genre = Genre::getGenreByName($name);

        return view('genres.show', compact('genre'));
    }
}
