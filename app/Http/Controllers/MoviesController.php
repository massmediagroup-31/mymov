<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Http\Requests\MovieStoreRequest;
use App\Http\Requests\MovieUpdateRequest;
use App\Models\Genre;
use App\Models\Movie;

class MoviesController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $movies = Movie::paginate(10);

        return view('movies.index', compact('movies'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $movie = Movie::find($id);

        return view('movies.show', compact('movie'));
    }

    /**
     *
     */
    public function create()
    {
        $genres = Genre::all();
        return view('movies.create', compact('genres'));
    }

    /**
     *
     */
    public function store(MovieStoreRequest $request)
    {
        $data = $request->all();
        $movie = new Movie($data);

        if (request()->image) {
            $imgName = Helper::saveImage(request()->image);
            $movie->image_url = $imgName;
        } else {
            $movie->image_url = 'not img';
        }

        $movie->save();
        $movie->genres()->attach($request->genres);

        return redirect()->route('movies.show', [$movie->id])
            ->with('success', 'Movie created');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $movie = Movie::find($id);
        $genres = Genre::all();
        $selectGenres = $movie->genres()->get()->pluck('id')->toArray();

        return view('movies.edit', compact('movie', 'genres', 'selectGenres'));
    }

    /**
     * @param MovieUpdateRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(MovieUpdateRequest $request, $id)
    {
        $data = $request->all();
        $movie = Movie::find($id);

        $movie->genres()->sync($request->genres);
        //dd($data);
        // Save image, delete prev image
        if (request()->image) {
            $imgName = Helper::saveImage(request()->image);
            Helper::deleteImage($movie->image_url);
            $movie->image_url = $imgName;
        }

        $movie->update($data);

        return redirect()->route('movies.index')
            ->with('success', 'Movie updated successfully');
    }
}
