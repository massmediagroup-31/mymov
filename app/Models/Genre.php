<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function movies()
    {
        return $this->belongsToMany(Movie::class, 'movies_genres', 'genres_id', 'movies_id');
    }

    public static function getGenreByName($name) {
        return Genre::where('name', '=', $name)->firstOrFail();
    }
}
