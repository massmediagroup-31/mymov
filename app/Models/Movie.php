<?php

namespace App\Models;

use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    use Searchable;

    protected $fillable = ['title', 'description', 'certificate', 'year', 'runtime', 'imdb_rating', 'metascore', 'rating'];
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function genres()
    {
        return $this->belongsToMany(Genre::class, 'movies_genres', 'movies_id', 'genres_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function stars()
    {
        return $this->belongsToMany(Star::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function directors()
    {
        return $this->belongsToMany(Director::class);
    }

    public function searchableAs()
    {
        return 'movies_index';
    }

    public function toSearchableArray()
    {
        $array = $this->toArray();


        return $array;
    }

}
