<?php

namespace App\Helpers;

use Illuminate\Support\Facades\File;

class Helper
{
    /**
     * @param $image
     * @return string
     */
    public static function saveImage($image)
    {
        $imageName = time().'.'.$image->getClientOriginalExtension();
        $image->move(public_path('images'), $imageName);

        return $imageName;
    }

    /**
     * @param $image
     */
    public static function deleteImage($image)
    {
        $image_path = public_path('images') .'/'. $image;
        if(File::exists($image_path)) {
            File::delete($image_path);
        }
    }
}