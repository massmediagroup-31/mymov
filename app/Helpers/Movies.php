<?php

namespace App\Helpers;

use DOMDocument;
use DOMXPath;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

class Movies
{
    /**
     * @param $url
     * @return array
     */
    public static function getRatedMoviesUrl($url)
    {
        $dom = new DomDocument;
        @$dom->loadHTMLFile($url);

        $xpath = new DomXPath($dom);

        $nodes = $xpath->query("//td[@class='titleColumn']//a/@href");
        $urls = [];
        foreach ($nodes as $i => $node) {
            $url = explode('/', $node->nodeValue);

            // take only 'title' and 'tt_id'
            $urls[] = $url[1] . '/' . $url[2];
        }

        return $urls;
    }

    /**
     * @param $url
     * @return array
     */
    protected static function getMovieAttr($url)
    {
        $movieAttr = [];

        $search = new \Imdb\Title($url);
        $movieAttr['title'] = $search->orig_title() ? $search->orig_title() : $search->title();
        $movieAttr['year'] = $search->year();
        $imgName = self::saveImage($search->photo(false));

        $movieAttr['image_url'] = $imgName;
        $movieAttr['certificate'] = '16+';
        $movieAttr['runtime'] = $search->runtimes()[0]['time'];
        $movieAttr['imdb_rating'] = $search->rating();
        $movieAttr['description'] = $search->plotoutline();
        $movieAttr['metascore'] = $search->metacriticRating() ? $search->metacriticRating() : 0;
        $movieAttr['rating'] = $search->rating();

        return $movieAttr;
    }

    /**
     * @return array
     */
    public static function getTop250()
    {
        $urls = self::getRatedMoviesUrl("https://www.imdb.com/chart/top?sort=ir,desc&mode=simple&page=1");

        $films = [];

        foreach ($urls as $i => $url) {
            $films[] = self::getMovieAttr($url);
            //if ($i == 1) break;
        }
        return $films;
    }

    public static function saveImage($url)
    {
        $filename = basename($url);
        Image::make($url)->save(public_path('images/' . $filename));
        return $filename;
    }
}