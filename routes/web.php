<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
//Route::get('/show/{id}', 'MoviesController@show')->name('movies.show');

Route::resource('movies', 'MoviesController');

Route::get('/genres/{name}', 'GenresController@show')->name('genres.show');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
