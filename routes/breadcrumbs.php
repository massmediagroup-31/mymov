<?php

Breadcrumbs::register('home', function ($breadcrumbs) {
    $breadcrumbs->push('Home', route('home'));
});
Breadcrumbs::register('movies.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Movies', route('movies.index'));
});

Breadcrumbs::register('movies.show', function ($breadcrumbs, $movie) {
    $breadcrumbs->parent('movies.index');
    $breadcrumbs->push($movie->title, route('movies.show',  $movie));
});

Breadcrumbs::register('movies.edit', function ($breadcrumbs, $movie) {
    $breadcrumbs->parent('movies.index');
    $breadcrumbs->push($movie->title . '(show)', route('movies.show',  $movie));
    $breadcrumbs->push($movie->title, route('movies.edit',  $movie));
});
