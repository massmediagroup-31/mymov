{{ Form::textarea($title, $value,
    ['placeholder' => ucfirst($title), 'class' => 'form-control', 'required' => $required]
)}}