{{ Form::text($title, $value,
    ['placeholder' => $placeholder ?? ucfirst($title) , 'class' => 'form-control', 'required' => $required,
    'value' => $value ]
)}}