@php
        $i = 1
@endphp
<div class="col-md-2">
    <h3>Random films</h3>
            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                            @foreach($lastMovies as $movie)
                                    <div class="carousel-item @if($i == 1) active @endif">
                                            <a href="/movies/{{ $movie->id }}"><img class="d-block w-100" src="/images/{{ $movie->image_url }}"
                                                    alt="Slide"></a>
                                    </div>
                                    @php
                                            $i += 1
                                    @endphp
                            @endforeach
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                    </a>
            </div>
</div>
