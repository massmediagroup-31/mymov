@extends('layouts.app')

@section('content')
    <div class="container" id="app">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h3>{{ $genre->title }}</h3>
                <ul>
                    @foreach($genre->movies as $movie)
                        <li>
                            <h4><a href="{{ route('movies.show', $movie->id) }}">{{ $movie->title }}({{ $movie->year }}
                                    )</a></h4>
                            <a href="{{ route('movies.show', $movie->id) }}"></a>
                            <a href="{{ route('movies.show', $movie->id) }}" target="_blank">
                                {{ Html::image('/images/' . $movie->image_url, 'a picture', array( 'height' => 500)) }}
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
@endsection