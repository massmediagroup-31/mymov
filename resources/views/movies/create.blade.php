@extends('layouts.app')

@section('content')
    <div class="container" id="app">
        <div class="row justify-content-center">
            <div class="col-md-8">
                {{ Form::open(array('route' => ['movies.store'] , 'method' => 'POST', 'files' => true)) }}
                @if (count($errors))
                    @component('layouts.components.alert')
                    @endcomponent
                @endif
                @if (session('success'))
                    @component('layouts.components.success')
                    @endcomponent
                @endif
                <div class="card text-center">
                    <div class="card-header">
                        @component('layouts.components.input',
                        ['title' => 'title', 'required' => 'required', 'value' => old('title')])
                        @endcomponent
                    </div>
                    <div class="card-header">
                        @component('layouts.components.select',
                        ['title' => 'genres[]', 'elements'=> $genres->pluck('name', 'id'), 'selected' => '','required' => 'required',
                        'value' => '', 'multiple' => 'multiple'])
                        @endcomponent
                    </div>
                    <div class="card-header">
                        @component('layouts.components.input',
                        ['title' => 'year', 'required' => 'required', 'value' => ''])
                        @endcomponent
                    </div>
                    <div class="card-header">
                        @component('layouts.components.select',
                        ['title' => 'certificate', 'elements' => ['6+', '16+', '18+'], 'selected' => '', 'multiple' => ''])
                        @endcomponent
                    </div>
                    <div class="card-header">
                        @component('layouts.components.input',
                        ['title' => 'runtime', 'required' => 'required', 'value' => ''])
                        @endcomponent
                    </div>
                    <div class="card-header">
                        @component('layouts.components.input',
                        ['title' => 'imdb_rating', 'required' => 'required', 'value' => ''])
                        @endcomponent
                    </div>
                    <div class="card-header">
                        @component('layouts.components.input',
                        ['title' => 'metascore', 'required' => 'required', 'value' => ''])
                        @endcomponent
                    </div>
                    <div class="card-header">
                        @component('layouts.components.input',
                        ['title' => 'rating', 'required' => 'required', 'value' => ''])
                        @endcomponent
                    </div>
                    <div class="card-body">
                        @component('layouts.components.textarea',
                        ['title' => 'description', 'required' => 'required', 'value' => ''])
                        @endcomponent
                    </div>
                    <div class="card-body">
                        <h4>Poster</h4>
                        <a href="../../images/" target="_blank">
                            {{ Html::image('/images/' . '', 'a picture', array( 'height' => 500)) }}
                        </a>
                    </div>
                    <div class="card-body">
                        @component('layouts.components.file',
                        ['title' => 'image'])
                        @endcomponent
                    </div>
                </div>
                <br>
                {{ Form::submit('Create',  ['class' => 'btn btn-primary']) }}
                {{ Form::close() }}
            </div>
        </div>
    </div>
@endsection