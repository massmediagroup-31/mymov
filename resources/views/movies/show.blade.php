@extends('layouts.app')

@section('content')
    <div class="col-md-10">
        <div class="row-full">
            {{ Breadcrumbs::render('movies.index', $movie) }}
        </div>
                @if (count($errors))
                    @component('layouts.components.alert')
                    @endcomponent
                @endif

                @if (session('success'))
                    @component('layouts.components.success')
                    @endcomponent
                @endif
                <div>
                    <a href="/images/{{ $movie->image_url }}" target="_blank">
                        {{ Html::image('/images/' . $movie->image_url, 'a picture', array( 'height' => 500)) }}
                    </a>
                </div>
                <a href="{{ route('movies.edit', $movie->id) }}" >Edit</a>
                <h3> {{ $movie->title }} ({{ $movie->year }})</h3>
                <h5>{{ $movie->description }}</h5>
                @foreach($movie->genres as $genre)
                    {{ $loop->first ? '' : ',' }}
                    <a href="/genres/{{ mb_strtolower($genre->name)  }}">{{ $genre->name  }}</a>
                @endforeach
            </div>
@endsection