@extends('layouts.app')

@section('content')
    <div class="col-md-10">
        <div class="row-full">
            {{ Breadcrumbs::render('movies.index', $movie) }}
        </div>
                {{ Form::open(array('route' => ['movies.update', $movie->id] , 'method' => 'PUT', 'files' => true)) }}
                @if (count($errors))
                    @component('layouts.components.alert')
                    @endcomponent
                @endif
                @if (session('success'))
                    @component('layouts.components.success')
                    @endcomponent
                @endif
                <div class="card text-center">
                    <div class="card-header">
                        @component('layouts.components.input',
                        ['title' => 'title', 'required' => 'required', 'value' => $movie->title])
                        @endcomponent
                    </div>
                    <div class="card-header">
                        @component('layouts.components.select',
                        ['title' => 'genres[]', 'elements'=> $genres->pluck('name', 'id'), 'selected' => $selectGenres,'required' => 'required',
                        'value' => $movie->title, 'multiple' => 'multiple'])
                        @endcomponent
                    </div>
                    <div class="card-header">
                        @component('layouts.components.input',
                        ['title' => 'year', 'required' => 'required', 'value' => $movie->year])
                        @endcomponent
                    </div>
                    <div class="card-header">
                        @component('layouts.components.select',
                        ['title' => 'certificate', 'elements' => ['6+', '16+', '18+'], 'selected' => $movie->certificate, 'multiple' => ''])
                        @endcomponent
                    </div>
                    <div class="card-body">
                        @component('layouts.components.textarea',
                        ['title' => 'description', 'required' => 'required', 'value' => $movie->description])
                        @endcomponent
                    </div>
                    <div class="card-body">
                        <h4>Poster</h4>
                        <a href="../../images/{{ $movie->image_url }}" target="_blank">
                            {{ Html::image('/images/' . $movie->image_url, 'a picture', array( 'height' => 500)) }}
                        </a>
                    </div>
                    <div class="card-body">
                        @component('layouts.components.file',
                        ['title' => 'image'])
                        @endcomponent
                    </div>
                </div>
                <br>
                {{ Form::submit('Create',  ['class' => 'btn btn-primary']) }}
                {{ Form::close() }}
            </div>

@endsection