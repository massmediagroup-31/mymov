@extends('layouts.app')

@section('content')


        <div class="col-md-10">
            <div class="row-full">
                {{ Breadcrumbs::render('movies.index', $movies) }}
            </div>
            @if (count($errors))
                @component('layouts.components.alert')
                @endcomponent
            @endif

            @if (session('success'))
                @component('layouts.components.success')
                @endcomponent
            @endif

            @foreach($movies as $movie)
                <div class="card text-center">
                    <div class="card-header">
                        <a href="{{ route('movies.show', $movie->id) }}" target="_blank">
                            <h3> {{ $movie->title }} ({{ $movie->year }}) </h3>
                        </a>
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">
                            <a href="images/{{ $movie->image_url }}" target="_blank">
                                {{ Html::image('/images/' . $movie->image_url, 'a picture', array( 'height' => 500)) }}
                            </a>
                        </h5>
                        <p class="card-text">{{ $movie->description }}</p>
                    </div>
                </div>
                <br>
            @endforeach
            {{ $movies->links() }}
        </div>

@endsection